# Assembly Checklist (GPC case)
1. Remove center foam block
2. Attach both landing gear legs to the aircraft body and lock them secure
3. Remove aircraft from the case by the landing gear, taking care not to catch the propellers.
4. Set the aircraft down on the landing gear
5. Unfold the arms, taking care not to catch the propellers
6. Install batteries and secure latch

# Pre-Flight Inspection

# Payloads

# Packing Checklist